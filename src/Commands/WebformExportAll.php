<?php
namespace Drupal\webform_export_all\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush command file.
 */
class WebformExportAll extends DrushCommands {

  /**
   * A custom Drush command to export submissions for all webforms.
   *
   * @command webform_export_all
   * @aliases we_all
   */
  public function webformExportAll() {
    ini_set('memory_limit', '-1');
    $private_dir = \Drupal::service('file_system')->realpath("private://");
    if ($private_dir) {
      $webform_ids = \Drupal::entityQuery('webform')
        ->execute();
      $dir = $private_dir . '/webform_export';
      \Drupal::service('file_system')->prepareDirectory($dir, \Drupal\Core\File\FileSystemInterface::CREATE_DIRECTORY);
      foreach ($webform_ids as $webform_id) {
        if (!empty($webform_id)) {
          $webform = \Drupal\webform\Entity\Webform::load($webform_id);
          $submission_exporter = \Drupal::service('webform_submission.exporter');
          $export_options = $submission_exporter->getDefaultExportOptions();

          $export_options['delimiter'] = ';';
          $export_options['multiple_delimiter'] = ',';

          // @todo in case we need a date range.
          /*$export_options['range_type'] = 'date';
          $export_options['range_start'] = '2018-02-18';
          $export_options['range_end'] = '2018-02-26';*/

          $submission_exporter->setWebform($webform);
          $submission_exporter->setExporter($export_options);
          $submission_exporter->generate();
          $file_path = $submission_exporter->getExportFilePath();

          $file_contents = file_get_contents($file_path);
          file_put_contents($private_dir . '/webform_export/' . $webform_id . '.csv', $file_contents);
          unlink($file_path);
          $this->output()->writeln('Exported submissions for webform ' . $webform_id . '.');
        }
      }
      $this->output()->writeln('All submissions exported.');
    }
    else {
      $this->output()->writeln('You need to define private files path.');
    }
  }
}
